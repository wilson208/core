<?php
/**
 * Created by PhpStorm.
 * User: Wilson
 * Date: 05/01/2018
 * Time: 22:43
 */

namespace Wilson208\Core\Test;


use Wilson208\Core\Application\UseCases\UseCase;

class TestUseCase extends UseCase
{
    /** @var  TestServiceInterface */
    public $testService;

    public function __construct(TestServiceInterface $testService)
    {
        $this->testService = $testService;
    }

    public function execute(){}
}