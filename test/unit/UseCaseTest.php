<?php

namespace Wilson208\Core;

use Wilson208\Core\Exceptions\ServiceNotRegisteredException;
use Wilson208\Core\Test\TestService;
use Wilson208\Core\Test\TestServiceInterface;
use Wilson208\Core\Test\TestUseCase;
use Wilson208\Core\Test\UnitTest;

abstract class UseCaseTest extends UnitTest
{
    public function testUseCaseCreate(){

        $this->verifyThrowsException(ServiceNotRegisteredException::class, function(){
            TestUseCase::create();
        });

        Application::instance()->registerService(TestServiceInterface::class, new TestService());
        $useCase = TestUseCase::create();
        verify($useCase)->isInstanceOf(TestUseCase::class);
        verify($useCase->testService)->isInstanceOf(TestServiceInterface::class);
    }
}