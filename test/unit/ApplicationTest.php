<?php

namespace Wilson208\Core;

use Wilson208\Core\Exceptions\ServiceNotRegisteredException;
use Wilson208\Core\Test\TestService;
use Wilson208\Core\Test\TestServiceInterface;
use Wilson208\Core\Test\UnitTest;

class ApplicationTest extends UnitTest
{
    public function testServiceRegistration()
    {
        $application = Application::instance();
        $this->verifyThrowsException(ServiceNotRegisteredException::class, function() use ($application){
            $application->getServiceForInterface(TestServiceInterface::class);
        });

        $application->registerService(TestServiceInterface::class, new TestService());
        verify($application->getServiceForInterface(TestServiceInterface::class))->isInstanceOf(TestService::class);
    }
}