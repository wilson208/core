<?php

namespace Wilson208\Core;

use Wilson208\Core\Exceptions\InvalidSettingsClassException;
use Wilson208\Core\Exceptions\ServiceDoesNotImplementInterfaceException;
use Wilson208\Core\Exceptions\ServiceNotRegisteredException;
use Wilson208\Core\Exceptions\SettingsNotRegisteredException;
use Wilson208\Core\Services\ServiceInterface;
use Wilson208\Core\Services\ServiceSettings;
use Wilson208\Core\Traits\SingletonTrait;

class Application
{
    use SingletonTrait;

    public $underTest = false;

    public $developerMode = true;

    private $registeredServicesForInterfaces = [];

    private $registeredSettingsForSettingsClass = [];

    public function registerService($interfaceClass, $service, $validate = true){
        if(!$validate || $service instanceof $interfaceClass){
            $this->registeredServicesForInterfaces[$interfaceClass] = $service;
        } else {
            throw new ServiceDoesNotImplementInterfaceException();
        }

    }

    public function registerSettings($settingsClassName, $settings){
        if($settings instanceof ServiceSettings){
            $this->registeredSettingsForSettingsClass[$settingsClassName] = $settings;
        } else {
            throw new InvalidSettingsClassException();
        }

    }

    public function getServiceForInterface($interfaceClass){
        if(!array_key_exists($interfaceClass, $this->registeredServicesForInterfaces)){
            throw new ServiceNotRegisteredException();
        }

        return $this->registeredServicesForInterfaces[$interfaceClass];
    }

    public function getSettingsForClass($settingsClass){
        if(!array_key_exists($settingsClass, $this->registeredSettingsForSettingsClass)){
            throw new SettingsNotRegisteredException();
        }

        return $this->registeredSettingsForSettingsClass[$settingsClass];
    }

    /**
     * @return ServiceInterface[]
     */
    public function getRegisteredServices() : array {
        $services = [];

        foreach ($this->registeredServicesForInterfaces as $interface => $service){
            $services[] = $service;
        }

        return $services;
    }

    public function clearRegisteredServices() {
        $this->registeredServicesForInterfaces = [];
    }
}