<?php

namespace Wilson208\Core\Services;

use Wilson208\Core\Exceptions\ServiceDoesNotImplementInterfaceException;

class MultiService extends Service
{
    /** @var ServiceInterface */
    private $interfaceClass;

    /** @var Service[] */
    private $services;

    /**
     * MultiService constructor.
     * @param string $interfaceName
     * @param Service[] $services
     * @throws ServiceDoesNotImplementInterfaceException
     */
    function __construct(string $interfaceName, array $services)
    {
        $this->interfaceClass = $interfaceName;
        $this->services = [];
        foreach ($services as $service) {
            if($service instanceof $interfaceName){
                $this->services[] = $service;
            } else {
                throw new ServiceDoesNotImplementInterfaceException(get_class($service) . ' does not implement ' . $interfaceName);
            }
        }
    }

    function __call($name, $arguments)
    {
        $responses = [];
        foreach ($this->services as $service) {
            $responses[] = call_user_func_array(array($service, $name), $arguments);
        }

        return $responses;
    }
}