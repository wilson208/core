<?php

namespace Wilson208\Core\Services\DoctrineServiceBase;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\Setup;
use Wilson208\Core\Application;
use Wilson208\Core\Application\Entities\StorableEntity;
use Wilson208\Core\Services\Service;
use Wilson208\Core\Services\ServiceInterface;

abstract class DoctrineServiceBase extends Service implements ServiceInterface
{
    private $entityToModelMappings = [];

    protected $em;

    public function __construct()
    {
        $this->getEntityManager();

        $settings = $this->getSettings();
        if($settings->isTesting){
            $this->updateSchema();
        }
    }

    public function getEntityManager() : EntityManager {
        if(!$this->em || !$this->em->isOpen()){
            $this->addEntityToModelMappings();

            $settings = $this->getSettings();
            if($settings->isTesting){
                $connectionParams = array(
                    'path' => tempnam(sys_get_temp_dir(), 'shotshop_unit_test_db_'),
                    'driver' => 'pdo_sqlite',
                );
            } else {
                $connectionParams = array(
                    'dbname' => $settings->db,
                    'user' => $settings->username,
                    'password' => $settings->password,
                    'host' => $settings->host,
                    'port' => $settings->port,
                    'driver' => 'mysqli',
                );
            }

            $reader = new AnnotationReader();
            foreach ($this->entityToModelMappings as $entityClass => $modelClass) {
                AnnotationRegistry::loadAnnotationClass($modelClass);
            }

            AnnotationRegistry::registerLoader('class_exists');
            $annotationDriver = new AnnotationDriver($reader, [$this->modelClassDirectory()]);

            $config = Setup::createConfiguration(Application::instance()->developerMode);
            $config->setMetadataDriverImpl($annotationDriver);

            $conn = $this->getConnection($connectionParams);
            // obtaining the entity manager
            $this->em = EntityManager::create($conn, $config);
        }

        return $this->em;
    }

    private function getConnection(array $connectionParams) : Connection {
        return ConnectionManager::instance()->getConnection($connectionParams);
    }

    public function updateSchema(){
        $tool = new SchemaTool($this->getEntityManager());
        $tool->updateSchema($this->getEntityManager()->getMetadataFactory()->getAllMetadata(), true);
    }

    public function truncate(){
        $connection = $this->getEntityManager()->getConnection();
        $schemaManager = $connection->getSchemaManager();
        $tables = $schemaManager->listTables();
        $query = 'SET FOREIGN_KEY_CHECKS = 0; ';

        $connection->executeQuery($query);
        foreach($tables as $table) {
            $name = $table->getName();
            $query = 'TRUNCATE `' . $name . '`; ';
            $connection->executeQuery($query);
        }
    }

    function getServiceSettingsClassName(): string
    {
        return DoctrineServiceSettings::class;
    }

    abstract function addEntityToModelMappings();

    public function registerEntityToModel($entityClass, $modelClass){
        $this->entityToModelMappings[$entityClass] = $modelClass;
    }

    public function storeEntity(StorableEntity $entity) : StorableEntity {
        $entity->beforeSave();
        $modelClass = $this->entityToModelMappings[get_class($entity)];
        $model = $this->entityToModel($entity);

        if(!empty($model->id)) {
            $this->getEntityManager()->merge($model);
        } else {
            $this->getEntityManager()->persist($model);
        }

        try {
            $this->getEntityManager()->beginTransaction();
            $this->getEntityManager()->flush($model);
            $this->getEntityManager()->commit();

            $entity = $this->modelToEntity($model, $modelClass);
            return $entity;
        } catch (\Exception $e){
            $this->getEntityManager()->rollback();
            throw $e;
        }
    }

    public function getEntity($entityClass, $id) : StorableEntity {
        $modelClass = $this->entityToModelMappings[$entityClass];
        /** @var DoctrineModel $model */
        $model = $this->getEntityManager()->find($modelClass, $id);
        return $this->modelToEntity($model, $modelClass);
    }

    protected function entityToModel(StorableEntity $entity) : DoctrineModel {
        $class = $this->entityToModelMappings[get_class($entity)];

        if(empty($entity->id)){
            $model = new $class();
        } else {
            $model = $this->getEntityManager()->getReference($class, $entity->id);
        }

        foreach ($entity as $key => $value) {
            $model->$key = $value;
        }

        return $model;
    }

    protected function modelToEntity(DoctrineModel $model, $modelClass = false) : StorableEntity {
        $this->getEntityManager()->detach($model);

        if(!$modelClass) {
            $modelClass = get_class($model);
        }

        $modelClass = str_replace('DoctrineProxies\__CG__\\', '', $modelClass);
        $class = array_search($modelClass, $this->entityToModelMappings);

        if(!class_exists($class, true)){
            throw new \Exception('Model not found of type: ' . $class);
        }

        /** @var StorableEntity $entity */
        $entity = new $class();
        foreach ($model as $key => $value) {
            $entity->$key = $value;
        }

        return $entity;
    }

    protected function modelArrayToEntityArray(array $models) : array {
        $entities = [];
        foreach ($models as $model) {
            $entities[] = $this->modelToEntity($model);
        }

        return $entities;
    }

    protected function modelClassDirectory(){
        return dirname((new \ReflectionClass(static::class))->getFileName());
    }
}