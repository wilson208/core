<?php

namespace Wilson208\Core\Services\DoctrineServiceBase;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\MappedSuperclass */
abstract class DoctrineModel
{
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue */
    public $id;
}