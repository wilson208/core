<?php

namespace Wilson208\Core\Services\DoctrineServiceBase;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Wilson208\Core\Traits\SingletonTrait;

class ConnectionManager
{
    use SingletonTrait;

    private $connections = [];

    public function getConnection($connectionParams) : Connection {
        $paramsHash = md5(json_encode($connectionParams));
        if(!array_key_exists($paramsHash, $this->connections)){
            $config = new \Doctrine\DBAL\Configuration();
            $this->connections[$paramsHash] = DriverManager::getConnection($connectionParams, $config);
        }

        return $this->connections[$paramsHash];
    }

    /**
     * @return Connection[]
     */
    public function getConnections() : array {
        return array_values($this->connections);
    }
}