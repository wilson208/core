<?php

namespace Wilson208\Core\Services\DoctrineServiceBase;

use Wilson208\Core\Services\ServiceSettings;

class DoctrineServiceSettings extends ServiceSettings
{
    public $host;
    public $port = 3306;
    public $username;
    public $password;
    public $db;
}