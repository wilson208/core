<?php

namespace Wilson208\Core\Services\DoctrineServiceBase;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\MappedSuperclass */
class CreatedUpdatedDateTimeDoctrineModel extends DoctrineModel
{
    /** @ORM\Column(type="datetime", nullable=false) */
    public $created;

    /** @ORM\Column(type="datetime", nullable=false) */
    public $updated;
}