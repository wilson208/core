<?php

namespace Wilson208\Core\Services;

use Wilson208\Core\Application;

abstract class Service
{
    function getServiceSettingsClassName() : string {
        return ServiceSettings::class;
    }

    protected function getSettings() : ServiceSettings {
        return Application::instance()->getSettingsForClass($this->getServiceSettingsClassName());
    }
}