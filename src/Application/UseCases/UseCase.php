<?php

namespace Wilson208\Core\Application\UseCases;

use Wilson208\Core\Application;

abstract class UseCase
{
    /**
     * @return static
     */
    public static function create() {
        $reflector = new \ReflectionClass(static::class);

        if(!$reflector->getConstructor() || empty($reflector->getConstructor()->getParameters())){
            return $reflector->newInstance();
        }

        $args = [];
        $application = Application::instance();
        foreach ($reflector->getConstructor()->getParameters() as $param) {
            $args[] = $application->getServiceForInterface($param->getClass()->name);
        }

        return $reflector->newInstanceArgs($args);
    }
}