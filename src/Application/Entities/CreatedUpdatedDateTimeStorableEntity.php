<?php

namespace Wilson208\Core\Application\Entities;

abstract class CreatedUpdatedDateTimeStorableEntity extends StorableEntity
{
    /** @var \DateTime */
    public $created;

    /** @var \DateTime */
    public $updated;

    public function beforeSave()
    {
        parent::beforeSave();

        $now = new \DateTime();
        $now->setTimestamp(strtotime('now'));
        if(!$this->created instanceof \DateTime){
            $this->created = $now;
        }
        $this->updated = $now;
    }

    public function cleansePrivatePropertiesBeforeDisplay()
    {
        parent::cleansePrivatePropertiesBeforeDisplay();
        unset($this->created);
        unset($this->updated);
    }
}