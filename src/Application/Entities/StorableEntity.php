<?php

namespace Wilson208\Core\Application\Entities;

abstract class StorableEntity extends Entity
{
    /** @var int */
    public $id;

    public function beforeSave(){}
}