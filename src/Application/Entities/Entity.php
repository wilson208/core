<?php

namespace Wilson208\Core\Application\Entities;

abstract class Entity
{
    public function cleansePrivatePropertiesBeforeDisplay() {}
}