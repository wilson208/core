<?php

namespace Wilson208\Core;


use Wilson208\Core\Traits\SingletonTrait;

class Settings
{
    use SingletonTrait;

    public $mysqlHost = '127.0.0.1';
    public $mysqlPort = 3306;
    public $mysqlUser = 'docker';
    public $mysqlPass = 'docker';
    public $mysqlDatabase = 'docker';
    public $mysqlTablePrefix = 'tmp_';

    public $elasticSearchHost = '127.0.0.1';
    public $elasticSearchPort = '127.0.0.1';

    /**
     * Instead of a constructor, put your
     * initialisation code here.
     */
    public function init() {
//        $this->mysqlHost = getenv('MYSQL_HOST');
//        $this->mysqlPort = getenv('MYSQL_PORT');
//        $this->mysqlUser = getenv('MYSQL_USER');
//        $this->mysqlPass = getenv('MYSQL_PASSWORD');
//        $this->mysqlDatabase = getenv('MYSQL_DATABASE');
//        $this->mysqlTablePrefix = getenv('MYSQL_TABLE_PREFIX');
    }
}