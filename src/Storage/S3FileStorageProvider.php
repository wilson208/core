<?php
/**
 * Created by PhpStorm.
 * User: Wilson
 * Date: 27/12/2017
 * Time: 01:18
 */

namespace Wilson208\Core\Storage;


use Aws\S3\S3Client;
use Wilson208\Core\Exceptions\NoProviderRegisteredForCategoryException;

class S3FileStorageProvider extends StorageProvider
{
    /** @var string */
    public $bucket;

    /** @var S3Client */
    public $client;

    public $categoryFolderMapping = [];

    private $cachedDownloads = [];

    public function storeFile(UploadedFile $file, string $category, $deleteExisting = true, $idPrefix = ''): string
    {
        if (!array_key_exists($category, $this->categoryFolderMapping)) {
            throw new NoProviderRegisteredForCategoryException();
        }

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($finfo, $file->localPath);
        finfo_close($finfo);

        $folder = $this->categoryFolderMapping[$category];
        $id = $idPrefix . uniqid();

        $path = "$folder/$id";
        $this->client->putObject([
            'Bucket' => $this->bucket,
            'Key' => $path,
            'SourceFile' => $file->localPath,
            'ACL' => 'public-read',
            'StorageClass' => 'REDUCED_REDUNDANCY',
            'ContentType' => $mimeType,
            'ContentDisposition' => 'attachment;filename=' . $id . '_' . $file->originalName
        ]);

        return $id;
    }

    public function downloadToFile(string $category, string $id, string $downloadTo)
    {
        try {
            if (array_key_exists($category . $id, $this->cachedDownloads)) {
                copy($this->cachedDownloads[$category . $id], $downloadTo);
                return;
            }
        } catch (\Exception $e) {}

        $folder = $this->categoryFolderMapping[$category];
        $path = "$folder/$id";
        $this->client->getObject([
            'Bucket' => $this->bucket,
            'Key' => $path,
            'SaveAs' => $downloadTo
        ]);
        $this->cachedDownloads[$category . $id] = $downloadTo;
    }

    public function getUrl(string $category, string $id): string
    {
        $folder = $this->categoryFolderMapping[$category];
        $path = "$folder/$id";
        return $this->client->getObjectUrl($this->bucket, $path);
    }
}