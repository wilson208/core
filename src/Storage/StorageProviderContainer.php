<?php

namespace Wilson208\Core\Storage;

use Wilson208\Core\Exceptions\NoProviderRegisteredForCategoryException;
use Wilson208\Core\Traits\SingletonTrait;

class StorageProviderContainer
{
    use SingletonTrait;

    private $categoryProviders = [];

    public function registerProviderForCategory(string $category, StorageProvider $provider) {
        $this->categoryProviders[$category] = $provider;
    }

    public function getProviderForCategory(string $category) : StorageProvider {
        if(!array_key_exists($category, $this->categoryProviders)){
            throw new NoProviderRegisteredForCategoryException();
        }

        return $this->categoryProviders[$category];
    }

    public function storeFile(string $category, UploadedFile $file, $idPrefix = '') : Asset {
        $provider = $this->getProviderForCategory($category);

        $id = $provider->storeFile($file, $category, true, $idPrefix);
        return new Asset($category, $id, $file->originalName);
    }

    public function downloadToFile(string $category, string $id, string $downloadTo){
        $provider = $this->getProviderForCategory($category);
        $provider->downloadToFile($category, $id, $downloadTo);
    }

    public function moveAssetToDifferentCategory(Asset $originalAsset, $newCategory) : Asset {
        $tmpFile = tempnam(sys_get_temp_dir(), 'asset_');
        $this->downloadToFile($originalAsset->category, $originalAsset->id, $tmpFile);

        return $this->storeFile($newCategory, new UploadedFile($tmpFile, $originalAsset->filename));
    }

    public function getUrl(Asset $asset) : string {
        $provider = $this->getProviderForCategory($asset->category);
        return $provider->getUrl($asset->category, $asset->id);
    }
}