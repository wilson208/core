<?php

namespace Wilson208\Core\Storage;

use Wilson208\Core\Exceptions\Exception;

class UploadedFile
{
    public $localPath;
    public $originalName;

    public function __construct(string $localPath, string $originalName)
    {
        if(!file_exists($localPath)){
            throw new Exception('File does not exists: ' . $localPath);
        }

        $this->localPath = $localPath;
        $this->originalName = $originalName;
    }
}