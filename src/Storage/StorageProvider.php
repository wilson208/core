<?php

namespace Wilson208\Core\Storage;

use Wilson208\Core\Traits\SingletonTrait;

abstract class StorageProvider
{
    use SingletonTrait;

    abstract public function storeFile(UploadedFile $file, string $category, $deleteExisting = true, $idPrefix = '') : string;
    abstract public function downloadToFile(string $category, string $id, string $downloadTo);
    abstract public function getUrl(string $category, string $id) : string;
}