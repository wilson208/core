<?php

namespace Wilson208\Core\Storage;

class Asset
{
    public $category;
    public $id;
    public $filename;

    public function __construct($category, $id, $filename)
    {
        $this->category = $category;
        $this->id = $id;
        $this->filename = $filename;
    }

    public static function createFromToken(string $token) {
        echo "Token: $token\r\n";
        $token = json_decode(base64_decode($token), true);
        return new Asset($token['category'], $token['id'], $token['filename']);
    }

    public function getToken() : string
    {
        $token = [
            'id' => $this->id,
            'category' => $this->category,
            'filename' => $this->filename
        ];

        return base64_encode(json_encode($token));
    }
}