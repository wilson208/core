<?php

namespace Wilson208\Core\Storage;


class LocalFileStorageProvider extends StorageProvider
{
    public $dataFolder;

    public function storeFile(UploadedFile $file, string $category, $deleteExisting = true): string
    {
        $id = uniqid();
        $dest = rtrim($this->dataFolder, '/') . '/' . $category . '/' .  $id . '_' . $file->originalName;
        echo dirname($dest);
        mkdir(dirname($dest), 0777, true);

        copy($file->localPath, $dest);

        if ($deleteExisting) {
            unlink($file->localPath);
        }

        return $id;
    }
}